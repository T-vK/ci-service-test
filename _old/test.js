const axios = require('axios')

async function main() {
    setInterval(async ()=>{
        try {
            const response = await axios.get('http://ais-ms-mock:3000')
            console.log(response.data)
        } catch (error) {
            console.error(error)
        }
    }, 1000)
    setTimeout(()=>{process.exit()}, 30000)
}

main().catch(console.error)
